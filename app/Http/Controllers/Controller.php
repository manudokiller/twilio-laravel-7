<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;
use Auth;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  protected $channels = array();
  protected $service;

  //used for a Twilio API get without throw
  public function getWithoutThrow($url)
  {
    return Http::withBasicAuth(env('TWILIO_USERNAME'), env('TWILIO_PASSWORD'))->get($url);
  }

  //used for a Twilio API get
  public function get($url)
  {
    return Http::withBasicAuth(env('TWILIO_USERNAME'), env('TWILIO_PASSWORD'))->get($url)->throw()->json();
  }

  //used for a Twilio API post
  public function post($url, $parameters)
  {
    return Http::withBasicAuth(env('TWILIO_USERNAME'), env('TWILIO_PASSWORD'))->asForm()->post($url, $parameters)->throw()->json();
  }

  //used for a Twilio API delete
  public function delete($url)
  {
    return Http::withBasicAuth(env('TWILIO_USERNAME'), env('TWILIO_PASSWORD'))->delete($url)->throw()->json();
  }

  //returns an array with the basic information of a channel
  public function getBasicChannel($channel)
  {
    return [
      'unique_name' => $channel['unique_name'],
      'sid' => $channel['sid'],
      'date_created' => $channel['date_created'],
      'date_updated' => $channel['date_updated'],
      'messages_count' => $channel['messages_count'],
      'members_count' => $channel['members_count']
    ];
  }

  //returns an array with the basic information of a service
  public function getBasicService($service)
  {
    return [
      'friendly_name' => $service['friendly_name'],
      'date_created' => $service['date_created']
    ];
  }

  //returns an array with the basic information of a message
  public function getBasicMessage($message)
  {
    return [
      'body' => $message['body'],
      'time_created' => date('h:i:s', strtotime($message['date_created'])),
      'sid' => $message['sid'],
      'from' => $message['from']
    ];
  }

  //returns all members and messages from a channel
  public function getBasicChannelMembersAndMessages($channel_sid)
  {
    $channel_members = array();
    $channel_messages = array();
  	$messages = $this->get(env('TWILIO_CHANNELS_URL') . '/' . $channel_sid . '/Messages')['messages'];
  	if (!empty($messages)) {
  		$last_date;
  		foreach ($messages as $message) {
  			$date = date('Y-m-d', strtotime($message['date_created']));
  			if(empty($channel_messages)) {
  				$channel_messages = array_merge(array($date => array()), $channel_messages);
  				$last_date = $date;
  			}else{
					if($last_date < $date){
  					$channel_messages = array_merge(array($date => array()), $channel_messages);
  				}
  			}
  			array_unshift($channel_messages[$date], $this->getBasicMessage($message));
  		}
  	}
    $members = $this->get(env('TWILIO_CHANNELS_URL') . '/' . $channel_sid . '/Members')['members'];
    if (!empty($members)) {
      foreach ($members as $member) {
        array_unshift($channel_members, $member['identity']);
      }
    }
  	return [
      'channel_members' => $channel_members,
      'channel_messages' => $channel_messages,
      'channel_messages_keys' => array_keys($channel_messages)
    ];
  }

  //gets an array of all the channels 
  public function getAllChannels()
  {
    $channels = $this->get(env('TWILIO_CHANNELS_URL'))['channels'];
    if (!empty($channels)) {
      foreach ($channels as $channel) {
        array_push($this->channels, $this->getBasicChannel($channel));
      }
      sort($this->channels);
    }
  }

  //verifies if a Member exists in a Channel
  public function memberExists($channel_sid)
  {
  	$response = $this->getWithoutThrow(env('TWILIO_CHANNELS_URL') . '/' . $channel_sid . '/Members/' . Auth::user()->email);
  	return $response->successful();
  }

  //adds a member to a channel * if already exists, do nothing
  public function addMemberToChannel($channel_sid)
  {
  	if (!$this->memberExists($channel_sid)) {
  		$this->post(env('TWILIO_CHANNELS_URL') . '/' . $channel_sid . '/Members', ['Identity' => Auth::user()->email]);
  	}
  }

  //verifies if a User exists in the service
  public function userExists($email)
  {
    $response = $this->getWithoutThrow(env('TWILIO_SERVICES_SID_URL') . '/Users/' . $email);
    return $response->successful();
  }

  //adds a user to the service * if already exists, do nothing
  public function addUserToService($request)
  {
    if (!$this->userExists($request['email'])) {
      $this->post(env('TWILIO_SERVICES_SID_URL') . '/Users', ['Identity' => $request['email'], 'FriendlyName' => $request['name']]);
    }
  }
}