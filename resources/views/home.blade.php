@extends('layouts.app')

@section('style')
<style type="text/css">
    .fa-sign-in-alt{
        text-align: center;
        text-decoration: none;
        color: black;
    }
    .fa-sign-in-alt:hover{
        text-align: center;
        text-decoration: none;
        color: black;
        font-size: 20px;
    }
    .table{
        text-align: center;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="float-left"><h3>Welcome to {{ $service['friendly_name'] }}</h3></div>
                    <div class="float-right"><h5>Channels: {{ count($channels) }}</h5></div>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Members Count</th>
                                <th scope="col">Messages Count</th>
                                <th scope="col">Join Channel</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($channels as $channel)
                                <tr>
                                    <th scope="row">{{ $channel['unique_name'] }}</th>
                                    <td>{{ $channel['members_count'] }}</td>
                                    <td>{{ $channel['messages_count'] }}</td>
                                    <td>
                                        <a href="{{ url('chat', $channel['sid']) }}" title="Join Channel"><i class="fas fa-sign-in-alt"></i></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
