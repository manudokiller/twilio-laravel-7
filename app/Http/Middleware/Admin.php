<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    //Verifies if the auth user is admin 
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->admin) {
            return redirect('/');
        }
        return $next($request);
    }
}
