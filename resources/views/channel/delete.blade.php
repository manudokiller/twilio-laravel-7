@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-5">
			<div class="card">
				{!! Form::model($channel, ['route' => ['channel.destroy', $channel['sid']], 'method' => 'delete']) !!}
					<div class="card-header">
						<h3>Deleting the Channel:</h3>
					</div>
					<div class="card-body">
						{!! Form::token() !!}
						<div class="form-group">
							{!! Form::label('unique_name', 'Name:') !!}
							{!! Form::text('unique_name', $channel['unique_name'], ['class' => 'form-control', 'disabled']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('sid', 'Sid Code:') !!}
							{!! Form::text('sid', $channel['sid'], ['class' => 'form-control', 'disabled']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('messages_count', 'Messages Count:') !!}
							{!! Form::text('messages_count', $channel['messages_count'], ['class' => 'form-control', 'disabled']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('members_count', 'Members Count:') !!}
							{!! Form::text('members_count', $channel['members_count'], ['class' => 'form-control', 'disabled']) !!}
						</div>
					</div>
					<div class="card-footer">
						{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
						{!! link_to_route('channel.index', 'Exit', [], ['class' => 'btn btn-primary']) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection