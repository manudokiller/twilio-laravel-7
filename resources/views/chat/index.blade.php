@extends('layouts.app')

@section('nav_item')
    <li class="nav-item">
        <div class="nav-link">Messages: {{ $channel['messages_count'] }}</div>
    </li>
    <li class="nav-item dropdown">
        <a id="channelMembersDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>Members: {{ $channel['members_count'] }} <span class="caret"></span></a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="channelMembersDropdown">
             @foreach($channel_members as $channel_member)
                <div class="nav-link">{{ $channel_member }}</div>
            @endforeach
        </div>
    </li>
@endsection

@section('style')
<style type="text/css">
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
    }
    textarea{
        resize: none;
    }
    .chat-container {
        position: relative;
        padding: 20px;
    }
    .chat-left, .chat-right {
        display: flex;
        flex: 1;
        flex-direction: row;
        margin-bottom: 10px;
    }
    .chat-left {
        margin-right: 25%;
        margin-left: 0;
    }
    .chat-right {
        justify-content: flex-end;
        margin-left: 25%;
        margin-right: 0;
    }
    .chat-text {
        margin: 0 5px 0 5px;
        font-size: 1.25rem;
    }
    .chat-hour, .chat-name, .chat-date {
        color: #999999;
        font-size: .6rem;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        margin: 0 5px 0 5px;
        text-align: center;
    }
    .chat-right > .chat-text {
        text-align: right;
    }
    .chat-date {
        font-size: .75rem;
    }
    .chat-box {
        border-style: groove;
        border-color: rgba(0, 0, 0, 0.125);
    }
    .chat-date {
        border-style: dotted;
        border-right: none;
        border-left: none;
        border-top: none;
        border-color: rgba(0, 0, 0, 0.125);
        margin-bottom: 15px;
    }
    .float-right {
        margin-left: 10px;
        margin-bottom: 10px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="float-left"><h3>Welcome to {{ $service['friendly_name'] }}</h3></div>
                    <div class="float-right"><h5><b>Channel: </b>{{ $channel['unique_name'] }}</h5></div>
                </div>
                <div class="card-body">
                    <ul>
                        <div class="chat-container col-md-12">
                            <ul>
                                {{ Form::open(['route' => 'sendMessage', 'role' => 'form']) }}
                                    {{ Form::token() }}
                                    <input type="hidden" name="sid" value="{{ $channel['sid'] }}">
                                    <div class="form-group">
                                        {!! Form::textarea('message', '', ['class' => 'form-control', 'placeholder' => 'Type your message here...', 'rows' => '3']) !!}
                                    </div>
                                    {!! Form::submit('Send Message', ['class' => 'btn btn-primary']) !!}
                                {{ Form::close() }}
                            </ul>
                            <br>
                            <ul>
                                <div class="chat-box">
                                    @if($channel_messages)
                                        @foreach($channel_messages_keys as $key)
                                            @foreach($channel_messages[$key] as $message)
                                                @if($message['from'] == Auth::user()->email)
                                                    <li class="chat-left">
                                                        <div class="chat-hour">{{ $message['time_created'] }}</div>
                                                        <div class="chat-name">{{ $message['from'] }}</div>
                                                        <div class="chat-text">{{ $message['body'] }}</div>
                                                    </li>
                                                @else
                                                    <li class="chat-right">
                                                        <div class="chat-text">{{ $message['body'] }}</div>
                                                        <div class="chat-name">{{ $message['from'] }}</div>
                                                        <div class="chat-hour">{{ $message['time_created'] }}</div>
                                                    </li>
                                                @endif
                                            @endforeach
                                            <li class="chat-date">{{ $key }}</li>
                                        @endforeach
                                    @endif
                                </div>
                            </ul>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection