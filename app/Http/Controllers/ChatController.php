<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ChatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->service = $this->getBasicService($this->get(env('TWILIO_SERVICES_SID_URL')));
    }

    public function index($sid)
    {
        $this->addMemberToChannel($sid);
        $channel_members_and_messages = $this->getBasicChannelMembersAndMessages($sid);
        return view('chat.index')->with([
            'service' => $this->service,
            'channel' => $this->get(env('TWILIO_CHANNELS_URL') . '/' . $sid),
            'channel_members' =>  $channel_members_and_messages['channel_members'],
            'channel_messages' =>  $channel_members_and_messages['channel_messages'],
            'channel_messages_keys' =>  $channel_members_and_messages['channel_messages_keys']
        ]);
    }

    public function sendMessage(Request $request)
    {
        $request->validate([
            'message' => 'required|max:255'
        ]);
        $parameters = [
            'From' => Auth::user()->email,
            'Body' => $request['message']
        ];
        $this->post(env('TWILIO_CHANNELS_URL') . '/' . $request['sid'] . '/Messages', $parameters);
        return redirect()->route('chat', [$request['sid']]);

    }

}