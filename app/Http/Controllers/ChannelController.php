<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;

class ChannelController extends Controller
{
    //$this->get(env('TWILIO_SERVICES_SID_URL')) => returns the service 
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        $this->service = $this->getBasicService($this->get(env('TWILIO_SERVICES_SID_URL')));
    }

    // returns the BasicService and all BasicChannels
    public function index()
    {
        $this->getAllChannels();
        return view('channel.index')->with([
            'service' => $this->service,
            'channels' => $this->channels
        ]);
    }

    //creates a new Channel
    public function store(Request $request)
    {
        $this->post(env('TWILIO_CHANNELS_URL'), [
            'ServiceSid' => env('TWILIO_SERVICES_SID'),
            'UniqueName' => $request['unique_name']
        ]);
        return Redirect()->route('channel.index');
    }

    //returns to the view where a Channel can be deleted
    public function show($sid)
    {
        return view('channel.delete')->with([
            'channel' => $this->getBasicChannel($this->get(env('TWILIO_CHANNELS_URL') . '/' . $sid)),
            'service' => $this->service
        ]);
    }

    //returns to the view where the Channel can be updated
    public function edit($sid)
    {
        return view('channel.update')->with([
            'channel' => $this->getBasicChannel($this->get(env('TWILIO_CHANNELS_URL') . '/' . $sid)),
            'service' => $this->service
        ]);
    }

    //updates a Channel (only UniqueName)
    public function update(Request $request, $sid)
    {
        $this->post(env('TWILIO_CHANNELS_URL') . '/' . $sid, ['UniqueName' => $request['unique_name']]);
        return Redirect()->route('channel.index');
    }

    //deletes a Channel
    public function destroy($sid)
    {
        $this->delete(env('TWILIO_CHANNELS_URL') . '/' . $sid);
        return Redirect()->route('channel.index');
    }
}
