@extends('layouts.app')

@section('style')
<style type="text/css">
    .fa-edit, .fa-times-circle, .fa-check-circle{
        text-align: center;
        text-decoration: none;
        color: black;
    }
    .fa-edit:hover, .fa-times-circle:hover, .fa-check-circle:hover{
        text-align: center;
        text-decoration: none;
        color: black;
        font-size: 20px;
    }
    .table{
        text-align: center;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
                {{ Form::open(['route' => 'channel.store', 'role' => 'form']) }}
                    <div class="card-header">
                        <h3>Create a New Channel</h3>
                    </div>
                    <div class="card-body">
                        {{ Form::token() }}
                        <div class="form-group">
                            {!! Form::label('unique_name', 'Name:') !!}
                            {!! Form::text('unique_name', '', ['class' => 'form-control', 'placeholder' => 'Name for the new channel']) !!}
                        </div>
                    </div>
                    <div class="card-footer">
                        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="float-left"><h3>Welcome to {{ $service['friendly_name'] }}</h3></div>
                    <div class="float-right"><h5>Channels: {{ count($channels) }}</h5></div>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Date Created</th>
                                <th scope="col">Date Updated</th>
                                <th scope="col">Messages Count</th>
                                <th scope="col">Members Count</th>
                                <th scope="col">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($channels as $channel)
                                <tr>
                                    <th scope="row">{{ $channel['unique_name'] }}</th>
                                    <td>{{ $channel['date_created'] }}</td>
                                    <td>{{ $channel['date_updated'] }}</td>
                                    <td>{{ $channel['messages_count'] }}</td>
                                    <td>{{ $channel['members_count'] }}</td>
                                    <td>
                                        <a href="{{ route('channel.edit', $channel['sid']) }}" title="Edit"><i class="fas fa-edit"></i></a>
                                         - 
                                        <a href="{{ route('channel.show', $channel['sid']) }}" title="Delete"><i class="fas fa-times-circle"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
