<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->service = $this->getBasicService($this->get(env('TWILIO_SERVICES_SID_URL')));
    }

    //returns to home view the service and it's channels
    public function index()
    {
        $this->getAllChannels();
        return view('home')->with([
            'service' => $this->service,
            'channels' => $this->channels
        ]);
    }
}
